// Do not remove the include below
#include "Clocky.h"

static Ping ping(InterruptConstants::Interrupt_Zero);

static Music buzzer(GPIO::Digital::buzzer);

/* This is the fun part, writing the music!!! Enter your BPM (beats per minute), for tempo, into your array first,
 * then put the note value you want followed by the duration. Example: c4,q (that is middle c for a quarter note).
 * the list of available notes and durations can be found in the Music.h file */

static const Music::music_note_t sheet_music[] = {
		 {b4,q},  {b4,q},  {c5,q},  {d5,q},  {d5,q},  {c5,q},  {b4,q},  {a4,q},  {g4,q},  {g4,q},  {a4,q},  {b4,q},  {b4,q+e},  {a4,e},  {a4,h},
         {b4,q},  {b4,q},  {c5,q},  {d5,q},  {d5,q},  {c5,q},  {b4,q},  {a4,q},  {g4,q},  {g4,q},  {a4,q},  {b4,q},  {a4,q+e},  {g4,e},  {g4,h},
         {a4,q},  {a4,q},  {b4,q},  {g4,q},  {a4,q},  {b4,e},  {c5,e},  {b4,q},  {g4,q},  {a4,q},  {b4,e},  {c5,e},  {b4,q},    {a4,q},  {g4,q},  {a4,q},  {d4,h},
         {b4,q},  {b4,q},  {c5,q},  {d5,q},  {d5,q},  {c5,q},  {b4,q},  {a4,q},  {g4,q},  {g4,q},  {a4,q},  {b4,q},  {a4,q+e},  {g4,e},  {g4,h}};

static Music::song_t tune(120, sheet_music, (sizeof(sheet_music) / sizeof(Music::music_note_t)));

//The setup function is called once at startup of the sketch
void setup()
{
	// Inits and Begins
	GPIO::Init();
	Serial.begin(115200);
}

// The loop function is called in an endless loop
void loop()
{
	if (GPIO::Digital::ReadDigital(GPIO::Digital::h_button) == 0)
	{
	Serial.println("h_button pressed");
	}

	if (GPIO::Digital::ReadDigital(GPIO::Digital::m_button) == 0)
	{
	Serial.println("m_button pressed");
	}

	if (GPIO::Digital::ReadDigital(GPIO::Digital::a_button) == 0)
	{
	Serial.println("a_button pressed");
	}

	if (GPIO::Digital::ReadDigital(GPIO::Digital::t_button) == 0)
	{
	Serial.println("t_button pressed");
	}

	if (GPIO::Digital::ReadDigital(GPIO::Digital::snooze_button) == 0)
	{
	Serial.println("snooze_button pressed");
	}

//	// Heartbeat
//	static GPIO::Digital::digitalValue_t heartbeat_state = GPIO::Digital::Low;
//	static unsigned long HeartbeatTime = GetTimerMS();
//	if (ElapsedMS(HeartbeatTime, GetTimerMS()) > HEARTBEAT_PERIOD_MS)
//	{
//		HeartbeatTime = GetTimerMS();
//		GPIO::Digital::ToggleDigital(GPIO::Digital::buzzer, heartbeat_state);
//	}

	// Music Player
	buzzer.playMusic(tune);      //nameOfYourPiezo.playMusic(nameOfYourSong[], sizeof(nameOfYourSong), this is all you need to make some music, Enjoy!

	// Ping Sensor
	static unsigned long PingTime = GetTimerMS();
	if (ElapsedMS(PingTime, GetTimerMS()) > PING_PERIOD_MS)
	{
		PingTime = GetTimerMS();
		ping.SendPing(Ping::StartPing);
	}
	else if (ping.GetPingState() == Ping::Done)
	{
		uint32_t pulseWidth = ping.GetPulseWidth();
		if ((pulseWidth > 0) && (pulseWidth < Ping::PulseMax_us))
		{
			Serial.print(pulseWidth);
			Serial.print(" uSec = ");
			Serial.print(Ping::uSecondsToCM(pulseWidth));
			Serial.println(" cm");
		}
		ping.SendPing(Ping::EndPing);
	}
}
