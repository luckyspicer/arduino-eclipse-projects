#include "GPIO.h"

void GPIO::Init()
{
	// Set Digital Pin modes
	//Digital::SetDigitalMode(Digital::HeartbeatLED, Digital::Output);

	Digital::SetDigitalMode(Digital::h_button, Digital::Pullup);
	Digital::SetDigitalMode(Digital::m_button, Digital::Pullup);
	Digital::SetDigitalMode(Digital::a_button, Digital::Pullup);
	Digital::SetDigitalMode(Digital::t_button, Digital::Pullup);
	Digital::SetDigitalMode(Digital::snooze_button, Digital::Pullup);
}
