// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef Clocky_H_
#define Clocky_H_
#include "Arduino.h"
//add your includes for the project Clocky here
#include "StandardConstants.h"
#include "GPIO.h"
#include "Ping.h"
#include "Music.h"

//end of add your includes here
#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif
//add your definition for the project Clocky here
static const unsigned int HEARTBEAT_PERIOD_MS = 1000;
static const unsigned int PING_PERIOD_MS = 100;


//add your function definitions for the project Clocky here




//Do not add code below this line
#endif /* Clocky_H_ */
