#ifndef _GPIO_H_
#define _GPIO_H_

#include "Arduino.h"
#include "standardConstants.h"

namespace GPIO
{

namespace Digital
{

typedef enum {
	pwm0 =			0,
	pwm1 =			1,
	pwm2 =			2,
	pwm3 =			3,
	pwm4 =			4,
	pwm5 =			5,
	pwm6 =			6,
	pwm7 =			7,
	pwm8 =			8,
	pwm9 =			9,
	pwm10 =			10,
	pwm11 =			11,
	pwm12 =			12,
	pwm13 =			13,
} pwmPin_t;
inline void SetPWM(pwmPin_t pin, uint16_t val) { analogWrite(pin, val); }

typedef enum {
	High = 			HIGH,
	Low = 			LOW,
} digitalValue_t;

typedef enum {
	Input =			INPUT,
	Output =		OUTPUT,
	Pullup =		INPUT_PULLUP,
} digitalPinMode_t;

typedef enum {
	Serial_RX =  		0,
	Serial_TX =  		1,
	SonarPing =  		2,
	INT2XM =  			3,
	motor_latch_0 =  	4,
	left_motor =  		5,
	INT1XM =  			6,
	motor_latch_1 =  	7,
	motor_latch_2 =  	8,
	snooze_button =  	9,
	h_button = 			10,
	right_motor = 		11,
	motor_latch_3 = 	12,
	buzzer = 			13, // Also, HeartbeatLED
// These digital pins are only available on mega variants
#if defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	digital14 = 	14,
	digital15 = 	15,
	digital16 = 	16,
	digital17 = 	17,
	digital18 = 	18,
	digital19 =		19,
	digital20 = 	20,
	digital21 = 	21,
	digital22 =		22,
	digital23 =		23,
	digital24 =		24,
	digital25 =		25,
	digital26 =		26,
	digital27 =		27,
	digital28 =		28,
	digital29 =		29,
	digital30 = 	30,
	digital31 = 	31,
	digital32 = 	32,
	digital33 =		33,
	digital34 =		34,
	digital35 =		35,
	digital36 =		36,
	digital37 = 	37,
	digital38 = 	38,
	digital39 = 	39,
	digital40 = 	40,
	digital41 = 	41,
	digital42 = 	42,
	digital43 = 	43,
	digital44 = 	44,
	digital45 = 	45,
	digital46 = 	46,
	digital47 = 	47,
	digital48 = 	48,
	digital49 = 	49,
	digital50 = 	50,
	digital51 = 	51,
	digital52 = 	52,
	digital53 = 	53,
#endif
	DRDYG = 			A0,
	m_button = 			A1,
	a_button = 			A2,
	t_button = 			A3,
	LSM9DS0_SDA = 		A4,
	LSM9DS0_SCL = 		A5,
	digitalA6 = 		A6,
	digitalA7 = 		A7,
// These analog pins are only available on mega variants
#if defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	digitalA8 = 	A8,
	digitalA9 = 	A9,
	digitalA10 = 	A10,
	digitalA11 = 	A11,
	digitalA12 = 	A12,
	digitalA13 = 	A13,
	digitalA14 = 	A14,
	digitalA15 = 	A15
#endif
} digitalPin_t;
inline void SetDigitalMode(digitalPin_t pin, digitalPinMode_t mode) { pinMode(pin, mode); }
inline uint16_t ReadDigital(digitalPin_t pin) { return (digitalRead(pin)); }
inline void SetDigital(digitalPin_t pin, digitalValue_t val) { digitalWrite(pin, val); }
inline void ToggleDigital(digitalPin_t pin, digitalValue_t& state) { state = ((state == Low) ? High : Low); digitalWrite(pin, state); }
}; // namespace Digital

namespace Analog
{

typedef enum {
	analog0 =		0,
	analog1 =		1,
	SonarPing =		2,
	analog3 =		3,
	analog4 =		4,
	analog5 =		5,
	analog6 =		6,
	analog7 =		7,
// These analog pins are only available on mega variants
#if defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
	analog8 =		8,
	analog9 =		9,
	analog10 = 		10,
	analog11 =		11,
	analog12 = 		12,
	analog13 =		13,
	analog14 =		14,
	analog15 =		15
#endif
} analogPin_t;
inline uint16_t ReadADC(analogPin_t pin) { return (analogRead(pin)); }

typedef enum {
	Default = DEFAULT,
	Internal = INTERNAL,
#if defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__) || defined(__AVR_ATmega1284P__) || defined(__AVR_ATmega644P__)
	Internal1V1 = INTERNAL1V1,
	Internal2V56 = INTERNAL2V56
#endif
	External = EXTERNAL
} analogReference_t;
inline void SetADCReference(analogReference_t refType) { analogReference(refType); }

}; // namespace Analog

void Init(void);

}; //  namespace GPIO
#endif
