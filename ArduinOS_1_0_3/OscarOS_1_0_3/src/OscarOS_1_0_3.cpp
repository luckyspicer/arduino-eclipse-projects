// Do not remove the include below
#include "OscarOS_1_0_3.h"

#define DEBUG_BUILD
#ifndef DEBUG_BUILD
// Debug/Test local declarations
bool motorControl = false;
static uint32_t debugTimer = 0;
static int32_t debugLEncoderCounter = 0;
static int32_t debugREncoderCounter = 0;
Ping sonar(InterruptConstants::Interrupt_Five);
static L3GD20 gyro;
// Application Objects
static UIBoard gui;
static SpeedController motorController;
#else
AF_DCMotor LeftMotor(1, MOTOR12_64KHZ);
AF_DCMotor RightMotor(2, MOTOR12_64KHZ);
static L3GD20 gyro;
static SpeedController motorController;
static bool motorControl = false;
#endif

// OSCAR's Heartbeat!
static void Heartbeat(void *pvParameters)
{
	(void) pvParameters;

	while(true)
	{
		GPIO::Digital::SetDigital(GPIO::Digital::HeartbeatLED, GPIO::Digital::High);
		vTaskDelay(500);
		GPIO::Digital::SetDigital(GPIO::Digital::HeartbeatLED, GPIO::Digital::Low);
		vTaskDelay(3000);
	}
}

#ifndef DEBUG_BUILD
static void vPing(void *pvParameters)
{
	(void) pvParameters;

	while(true)
	{
		sonar.SendPing(Ping::StartPing);
		do
		{
			vTaskDelay(2);
		} while (sonar.GetPulseWidth() <=  Ping::PulseMin_us);
		XBee.print(F("Ping: "));
		XBee.print(sonar.GetPulseWidth());

		XBee.print(" 8:");
		XBee.print(GPIO::Analog::ReadADC(GPIO::Analog::analog8));	// Front Right

		XBee.print(" 9:");
		XBee.print(GPIO::Analog::ReadADC(GPIO::Analog::analog9));	// Front Center

		XBee.print(" 10:");
		XBee.print(GPIO::Analog::ReadADC(GPIO::Analog::analog10));	// Front left

		XBee.print(" 11:");
		XBee.print(GPIO::Analog::ReadADC(GPIO::Analog::analog11));	// Top IR

		XBee.print(" 13:");
		XBee.print(GPIO::Analog::ReadADC(GPIO::Analog::analog13));	// Back IR

		XBee.println();
		vTaskDelay(500);
	}
}

void vGUI(void *pvParameters)
{
	(void) pvParameters;

	while (true)
	{
		gui.Update();
		vTaskDelay(50);
	}
}

void vEncoderTest(void *pvParameters)
{
	(void) pvParameters;\

	while (true)
	{
		debugTimer = millis();
		motorControl = true;
		Serial.println(F("Time, Left Count, Right Count"));
		while ((millis() - debugTimer) < 10000)
		{
			int32_t tempLEncoderCounter = LMEncoder.GetCount();
			int32_t tempREncoderCounter = RMEncoder.GetCount();
			if ((tempLEncoderCounter != debugLEncoderCounter) ||
				(tempREncoderCounter != debugREncoderCounter))
			{
				debugLEncoderCounter = tempLEncoderCounter;
				debugREncoderCounter = tempREncoderCounter;
				Serial.print(millis());
				Serial.print(" L: ");
				Serial.print(LMEncoder.GetDirection());
				Serial.print(", ");
				Serial.print(GPIO::Digital::ReadDigital(GPIO::Digital::LEncoderDir));
				Serial.print(", ");
				Serial.print(debugLEncoderCounter);
				Serial.print(" R: ");
				Serial.print(RMEncoder.GetDirection());
				Serial.print(", ");
				Serial.print(GPIO::Digital::ReadDigital(GPIO::Digital::REncoderDir));
				Serial.print(", ");
				Serial.println(debugREncoderCounter);
			}
			motorController.DriveDistance(SpeedController::MMToPulses(300), SpeedController::Forward);
			vTaskDelay(3000);
			motorController.DriveDistance(SpeedController::MMToPulses(300), SpeedController::Backward);
			vTaskDelay(3000);
			motorController.RotateDistance(SpeedController::DegreeToPulses(90), SpeedController::Clockwise);
			vTaskDelay(3000);
		}
		Serial.println(F("End Encoder Printing"));
		motorControl = false;
		motorController.Stop();
		while (true)
		{
			vTaskDelay(500);
		}
	}
}

void vMotorControl(void *pvParameters)
{
	(void) pvParameters;

	while (true)
	{
		while (!motorControl)
		{
			vTaskDelay(10);
		}
		while (motorControl)
		{
			motorController.Update();
			vTaskDelay(10);
		}
		while (true)
		{
			vTaskDelay(500);
		}
	}
}

void vSpeechTest(void *pvParameters)
{
	(void) pvParameters;

	while (true)
	{
		Emic2.print('\n');             // Send a CR in case the system is already up
		while (Emic2.read() != ':') { vTaskDelay(10); }   	// When the Emic 2 has initialized and is ready, it will send a single ':' character, so wait here until we receive it
		Emic2.flush();                 	// Flush the receive buffer

		// Speak some text
		Emic2.print('S');
		Emic2.print(F("Hello, Anne. My name is Oscar Two Point Oh. I would like to sing you a song."));  // Send the desired string to convert to speech
		Emic2.print('\n');
		while (Emic2.read() != ':') { vTaskDelay(10); }   // Wait here until the Emic 2 responds with a ":" indicating it's ready to accept the next command

		vTaskDelay(500);

		// Sing a song
		Emic2.print(F("D1\n"));
		while (Emic2.read() != ':') { vTaskDelay(10); }   // Wait here until the Emic 2 responds with a ":" indicating it's ready to accept the next command

		vTaskDelay(5000);
	}
}

void vGyroTest(void *pvParameters)
{
	(void) pvParameters;

	while (true)
	{
		while (millis() < 12000)
		{
			vTaskDelay(500);
		}
		L3GD20::gyroData_t gyroData;
		gyro.ReadData();
		gyro.GetGyroData(gyroData);
		if ((gyroData.status & L3GD20::L3G_ZYXDA) != 0)
		{
			Serial.print(gyroData.status);
			Serial.print(" ");
			Serial.print(gyroData.temp);
			Serial.print(" ");
			Serial.print(gyroData.x + 41);
			Serial.print(" ");
			Serial.print(gyroData.y + 69);
			Serial.print(" ");
			Serial.println(gyroData.z - 152);
		}
		vTaskDelay(5);
	}
}
#else

//void vMinimumPWMTest(void *pvParameters)
//{
//	(void) pvParameters;
//
//	RMEncoder.Connect(InterruptConstants::PCINT_1, GPIO::Digital::REncoderDir);
//	LMEncoder.Connect(InterruptConstants::PCINT_0, GPIO::Digital::LEncoderDir);
//	while (true)
//	{
//		XBee.println("Minimum PWM Test");
//		XBee.println("LM PWM | RM PWM");
//		while ((LMEncoder.GetFrequency() == 0) || (RMEncoder.GetFrequency() == 0))
//		{
//			LeftMotor.setSpeed(lmSpeed);
//			RightMotor.setSpeed(rmSpeed);
//			LeftMotor.run(FORWARD);
//			RightMotor.run(FORWARD);
//			vTaskDelay(100);
//			++lmSpeed;
//			++rmSpeed;
//		}
//		LeftMotor.run(RELEASE);
//		RightMotor.run(RELEASE);
//		XBee.print(rmSpeed);
//		XBee.print(" ");
//		XBee.println(lmSpeed);
//		while (true)
//		{
//			vTaskDelay(500);
//		}
//	}
//	// Minimum PWM is 52, 47, 55, 53, 60 or something like that
//}

//void vStepInputPWMTest(void *pvParameters)
//{
//	(void) pvParameters;
//
//	RMEncoder.Connect(InterruptConstants::PCINT_1, GPIO::Digital::REncoderDir);
//	LMEncoder.Connect(InterruptConstants::PCINT_0, GPIO::Digital::LEncoderDir);
//	uint8_t lmSpeed = 100; // step input pwm
//	uint8_t rmSpeed = 100; // step input pwm
//	while (true)
//	{
//		uint32_t startTime = millis();
//		LeftMotor.setSpeed(lmSpeed);
//		RightMotor.setSpeed(rmSpeed);
//		LeftMotor.run(FORWARD);
//		RightMotor.run(FORWARD);
//		uint32_t lmFrequency = 0;
//		uint32_t rmFrequency = 0;
//		L3GD20::gyroData_t gyroData;
//		while (ElapsedMS(startTime, millis()) < 5000)
//		{
//			if(ElapsedMS(startTime, millis()) > 3000)
//			{
//				LeftMotor.run(RELEASE);
//				RightMotor.run(RELEASE);
//			}
//			uint32_t tempLMFrequency = LMEncoder.GetFrequency();
//			uint32_t tempRMFrequency = RMEncoder.GetFrequency();
//			gyro.ReadData();
//			gyro.GetGyroData(gyroData);
////			if ((tempLMFrequency != lmFrequency) ||
////				(tempRMFrequency != rmFrequency))
////			{
//				lmFrequency = tempLMFrequency;
//				rmFrequency = tempRMFrequency;
//				Serial.print(micros());
//				Serial.print(" ");
//				Serial.print(lmFrequency);
//				Serial.print(" ");
//				Serial.print(tempRMFrequency);
//				Serial.print(" ");
//				Serial.print(gyroData.x + 41);
//				Serial.print(" ");
//				Serial.print(gyroData.y + 69);
//				Serial.print(" ");
//				Serial.println(gyroData.z - 152);
//				vTaskDelay(5);
////			}
//		}
//		LeftMotor.run(RELEASE);
//		RightMotor.run(RELEASE);
//		while (true)
//		{
//			vTaskDelay(500);
//		}
//	}
//	// Minimum PWM is 52, 47, 55, 53, 60 or something like that
//}

void vMotorControl(void *pvParameters)
{
	(void) pvParameters;

	while (true)
	{
		while (!motorControl)
		{
			vTaskDelay(100);
		}
		while (motorControl)
		{
			motorController.Update();
			vTaskDelay(10);
		}
	}
}

void vCloseLoopStepInputPWMTest(void *pvParameters)
{
	(void) pvParameters;

	while (true)
	{

		uint32_t startTime = millis();
		uint32_t lmFrequency = 0;
		uint32_t rmFrequency = 0;
		L3GD20::gyroData_t gyroData;
		motorController.DriveSpeed(100, SpeedController::Forward);
		motorControl = true;
		while (ElapsedMS(startTime, millis()) < 5000)
		{
			if(ElapsedMS(startTime, millis()) > 3000)
			{
				motorController.Stop();
			}
			uint32_t tempLMFrequency = LMEncoder.GetFrequency();
			uint32_t tempRMFrequency = RMEncoder.GetFrequency();
			gyro.ReadData();
			gyro.GetGyroData(gyroData);
			lmFrequency = tempLMFrequency;
			rmFrequency = tempRMFrequency;
			Serial.print(micros());
			Serial.print(" ");
			Serial.print(lmFrequency);
			Serial.print(" ");
			Serial.print(tempRMFrequency);
			Serial.print(" ");
			Serial.print(gyroData.x + 41);
			Serial.print(" ");
			Serial.print(gyroData.y + 69);
			Serial.print(" ");
			Serial.println(gyroData.z - 152);
			vTaskDelay(5);
		}
		motorControl = false;
		motorController.Stop();
		while (true)
		{
			vTaskDelay(500);
		}
	}
}

#endif

void setup()
{
	// Inits and Begins
	GPIO::Init();
	Serial.begin(115200);
//	Emic2.begin(9600);		// Emic2 Text to speech module
	XBee.begin(38400);		// Max baud for Wireless (Xbee) 38400
	Wire.begin();
//	gui.Begin();
	motorController.Begin();
	if (!gyro.Init())
	{
		Serial.println(F("Failed to autodetect the gyro type!"));
	}
	gyro.EnableDefault();


	// Start Messages
//	Serial.println(F("Arduino FreeRTOS Test"));
//	XBee.println(F("Arduino Xbee Test!"));

	// Create the Heartbeat task (process)
	xTaskCreate( Heartbeat, (signed char*)"Heartbeat", configMINIMAL_STACK_SIZE, NULL, TOP_PRIORITY, NULL );
	// Create the OS Tasks (processes)
//	xTaskCreate( vGUI, (signed char*)"GUI", configMINIMAL_STACK_SIZE*3, NULL, NORMAL_PRIORITY, NULL );
//	xTaskCreate( vEncoderTest, (signed char*)"Encoders", configMINIMAL_STACK_SIZE*2, NULL, NORMAL_PRIORITY, NULL );
//	xTaskCreate( vMotorControl, (signed char*)"Motors", configMINIMAL_STACK_SIZE*2, NULL, NORMAL_PRIORITY, NULL );
//	xTaskCreate( vSpeechTest, (signed char*)"Speech", configMINIMAL_STACK_SIZE*1, NULL, NORMAL_PRIORITY, NULL );
//	xTaskCreate( vPing, (signed char*)"Ping", configMINIMAL_STACK_SIZE*3, NULL, NORMAL_PRIORITY, NULL );
//	xTaskCreate( vGyroTest, (signed char*)"Gyro", configMINIMAL_STACK_SIZE*2, NULL, NORMAL_PRIORITY, NULL );

	// Debug Tasks (processes)
//	xTaskCreate( vMinimumPWMTest, (signed char*)"MotorTest", configMINIMAL_STACK_SIZE, NULL, NORMAL_PRIORITY, NULL );
//	xTaskCreate( vStepInputPWMTest, (signed char*)"MotorTest", configMINIMAL_STACK_SIZE * 5, NULL, NORMAL_PRIORITY, NULL );
	xTaskCreate( vCloseLoopStepInputPWMTest, (signed char*)"MotorTest", configMINIMAL_STACK_SIZE * 5, NULL, NORMAL_PRIORITY, NULL );
	xTaskCreate( vMotorControl, (signed char*)"Motors", configMINIMAL_STACK_SIZE*2, NULL, NORMAL_PRIORITY, NULL );

	// Start the OS (scheduler)
	vTaskStartScheduler();
}

void loop(){}
