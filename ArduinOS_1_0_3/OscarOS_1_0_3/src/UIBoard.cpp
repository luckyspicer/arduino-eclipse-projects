#include "StandardConstants.h"
#include "UIBoard.h"
#include "GPIO.h"

UIBoard::UIBoard(): displayButtons(InterruptConstants::Interrupt_Four, InterruptConstants::Interrupt_Falling, GPIO::Digital::UpButton, GPIO::Digital::LeftButton, GPIO::Digital::RightButton, GPIO::Digital::DownButton, GPIO::Digital::SelectButton), timer(0)
{
	for (uint8_t idx = 0; idx < UIButtons::NumberButtons; ++idx)
	{
		buttonPressTimes[idx] = 0;
	}
};

void UIBoard::Begin()
{
	displayButtons.Begin();

	// Test of GLCD
	display.Init(INVERTED);
	display.ClearScreen();
	display.DrawBitmap(ArduinoIcon, 0,0, BLACK); //draw the bitmap at the given x,y position

	// Text areas
	for (uint8_t buttonIdx = 0; buttonIdx < UIButtons::NumberButtons; ++buttonIdx)
	{
		displayTextArea[buttonIdx].DefineArea(glcd::CenterX, buttonIdx * 8, 10, 1, System5x7, SCROLL_UP);
		displayTextArea[buttonIdx].SelectFont(System5x7, BLACK); // switch to fixed width system font
	}
	displayCliffArea.DefineArea(glcd::CenterX, glcd::Height - 16, 10, 1, System5x7, SCROLL_UP);
	displayTimeArea.DefineArea(glcd::CenterX, glcd::Height - 8, 10, 1, System5x7, SCROLL_UP);
}

void UIBoard::Update()
{
	// Print UI Button Information
	uint16_t buttonNum = displayButtons.WhichUIButtonPressed();
	if (buttonNum != UIButtons::NoButton)
	{
		displayTextArea[buttonNum].ClearArea();
		if (buttonNum ==  UIButtons::UpButton) {
			displayTextArea[buttonNum].printFlash(flashStr("Up"));
		} else if (buttonNum ==  UIButtons::LeftButton) {
			displayTextArea[buttonNum].printFlash(flashStr("Left"));
		} else if (buttonNum == UIButtons::RightButton) {
			displayTextArea[buttonNum].printFlash(flashStr("Right"));
		} else if (buttonNum == UIButtons::DownButton) {
			displayTextArea[buttonNum].printFlash(flashStr("Down"));
		} else if (buttonNum == UIButtons::SelectButton) {
			displayTextArea[buttonNum].printFlash(flashStr("Select"));
		}
		displayTextArea[buttonNum].CursorTo(6, 0);
		displayTextArea[buttonNum].print(++buttonPressTimes[buttonNum]);
	}

	// Print Cliff Sensor Information
	displayCliffArea.ClearArea();
	displayCliffArea.printFlash(flashStr("FC:  RC:  "));
	displayCliffArea.CursorTo(3, 0);
	displayCliffArea.print((GPIO::Digital::ReadDigital(GPIO::Digital::FrontCliff) == 0) ? 'F' : 'T');
	displayCliffArea.CursorTo(8, 0);
	displayCliffArea.print((GPIO::Digital::ReadDigital(GPIO::Digital::RearCliff) == 0) ? 'F' : 'T');

	// Print Time Information
	if (ElapsedMS(timer, millis()) > updateTimer_ms)
	{
		timer = millis();
		displayTimeArea.ClearArea();
		displayTimeArea.printFlash(flashStr("Time:"));
		displayTimeArea.print(timer/MS_PER_SEC);
	}
}
