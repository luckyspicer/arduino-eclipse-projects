#include "Ping.h"

// Static (Local) Declarations
static void isrPing(void);
static Ping *localPingPtr = NULL;

Ping::Ping(InterruptConstants::InterruptNumber_t inter_num): interruptNum(inter_num), pulseTimer(0), transitionTimer(0), pulseWidth(PulseMin_us), pingState(WaitForTrigger)
{
	triggerPin = (GPIO::Digital::digitalPin_t)InterruptConstants::GetInterruptPin(inter_num);
}

uint32_t Ping::SendPing(PingCmd_t cmd)
{
	if ((cmd == StartPing) && (pingState != WaitForTrigger))
	{
		return (0);
	}
	switch (pingState)
	{
	case WaitForTrigger:
		localPingPtr = this;
		pulseTimer = 0;
		pulseWidth = PulseMin_us;
		pingState = WaitForRising;
		GPIO::Digital::SetDigitalMode(GPIO::Digital::SonarPing, GPIO::Digital::Output);
		GPIO::Digital::SetDigital(triggerPin, GPIO::Digital::Low);
		delayMicroseconds(2);
		GPIO::Digital::SetDigital(triggerPin, GPIO::Digital::High);
		delayMicroseconds(5);
		GPIO::Digital::SetDigital(triggerPin, GPIO::Digital::Low);
		GPIO::Digital::SetDigitalMode(GPIO::Digital::SonarPing, GPIO::Digital::Input);
		transitionTimer = millis();
		attachInterrupt(interruptNum, isrPing, InterruptConstants::Interrupt_Rising);
		break;
	case WaitForRising:
		pulseTimer = micros();
		pingState = WaitForFalling;
		transitionTimer = millis();
		attachInterrupt(interruptNum, isrPing, InterruptConstants::Interrupt_Falling);
		break;
	case WaitForFalling:
		pulseWidth = ElapsedUS(pulseTimer, micros());
		detachInterrupt(interruptNum);
		pingState = WaitForTrigger;
		break;
	}
	return (1);
}

uint32_t Ping::GetPulseWidth()
{
	uint32_t returnVal = ((pulseWidth >= PulseMin_us) ? pulseWidth : PulseMin_us);
	if (ElapsedMS(transitionTimer, millis()) >= TransitionTimeout_ms)
	{
		detachInterrupt(interruptNum);
		pingState = WaitForTrigger;
		returnVal = PulseMax_us;
	}
	return (returnVal);
}

uint32_t Ping::GetPingState()
{
	return (pingState);
}

void isrPing()
{
	localPingPtr->SendPing(Ping::ContinuePing);
}
