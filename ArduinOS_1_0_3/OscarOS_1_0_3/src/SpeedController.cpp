#include "SpeedController.h"

SpeedController::SpeedController(): leftMotor(LMPort, LMFrequency), rightMotor(RMPort, RMFrequency),
									targetDirection(NoMotion), targetSpeed(0), targetDistance(0), targetRotation(0),
									updateController(false),
									controllerType(NoControl), lastControllerType(NoControl),
									LMPIDInput(NULL), RMPIDInput(NULL), LMPIDOutput(NULL), RMPIDOutput(NULL),
									Kp(0), Ki(0), Kd(0),
									LMPID(&LMPIDInput, &LMPIDOutput, &LMPIDSetpoint, Kp, Ki, Kd, DIRECT), RMPID(&RMPIDInput, &RMPIDOutput, &RMPIDSetpoint, Kp, Ki, Kd, DIRECT){}

void SpeedController::Begin()
{
	RMEncoder.Connect(InterruptConstants::PCINT_1, GPIO::Digital::REncoderDir);
	LMEncoder.Connect(InterruptConstants::PCINT_0, GPIO::Digital::LEncoderDir);
	LMPID.SetSampleTime(PIDSamplePeriod_ms);
	RMPID.SetSampleTime(PIDSamplePeriod_ms);
	LMPID.SetOutputLimits(MinSpeed, MaxSpeed);
	RMPID.SetOutputLimits(MinSpeed, MaxSpeed);
}

void SpeedController::Update()
{
	// If the controller type or setpoint has changed reset the controller
	if (updateController)
	{
		updateController = false;

		// Reset the PID controller
		LMEncoder.Reset();
		RMEncoder.Reset();
		LMPID.SetMode(MANUAL);
		RMPID.SetMode(MANUAL);
	}

	switch (controllerType)
	{
	case DistanceControl:
	case RotationControl:
	{
		// Get Data from the Encoders
		int32_t LMCount = -(LMEncoder.GetCount()); // Since this encoder is inverted relative to the RMEncoder
		int32_t RMCount = RMEncoder.GetCount();
		int64_t target = 0;
		// Set the Target and PID tuning based on the type of controller we are running
		if (controllerType == DistanceControl)
		{
			target = (int64_t)targetDistance;
			// Let's use the PID controller now (to track to the target position)
			LMPID.SetTunings(KpPos, KiPos, KdPos);
			RMPID.SetTunings(KpPos, KiPos, KdPos);
		} else if (controllerType == RotationControl){
			target = (int64_t)targetRotation;
			// Let's use the PID controller now (to track to the target rotation)
			LMPID.SetTunings(KpRot, KiRot, KdRot);
			RMPID.SetTunings(KpRot, KiRot, KdRot);
		}

		// Turn on the PID controller now
		LMPID.SetMode(AUTOMATIC);
		RMPID.SetMode(AUTOMATIC);
		LMPIDSetpoint = (double)target;
		RMPIDSetpoint = (double)target;
		// The direction we are trying to go is always considered positive
		LMPIDInput = (double)(((targetDirection == Backward) || (targetDirection == Counterclockwise)) ? -LMCount : LMCount);
		RMPIDInput = (double)((((targetDirection == Forward) || (targetDirection == Counterclockwise))) ? RMCount : -RMCount);

		// Computer the Output using the PIDs
		LMPID.Compute();
		RMPID.Compute();
		// The direction we are trying to go is always considered positive
		int16_t lmOutput = (int16_t)(((targetDirection == Backward) || (targetDirection == Counterclockwise)) ? -LMPIDOutput : LMPIDOutput);
		int16_t rmOutput = (int16_t)((((targetDirection == Forward) || (targetDirection == Counterclockwise))) ? RMPIDOutput : -RMPIDOutput);

		// Set the left motor based on the PID output
		leftMotor.setSpeed(abs(lmOutput));
		if ((lmOutput > 0) && (lmOutput > MinMovingSpeed))
		{
			leftMotor.run(FORWARD);
		}
		else if ((lmOutput < 0) && (abs(lmOutput) > MinMovingSpeed))
		{
			leftMotor.run(BACKWARD);
		}
		else
		{
			leftMotor.run(RELEASE);
			leftMotor.setSpeed(0);
		}

		// Set the right motor based on the PID output
		rightMotor.setSpeed(abs(rmOutput));
		if ((rmOutput > 0) && (rmOutput > MinMovingSpeed))
		{
			rightMotor.run(FORWARD);
		}
		else if ((rmOutput < 0) && (abs(rmOutput) > MinMovingSpeed))
		{
			rightMotor.run(BACKWARD);
		}
		else
		{
			rightMotor.run(RELEASE);
			rightMotor.setSpeed(0);
		}
	}
		break;
	case SpeedControl:
	{
		// Get Data from the Encoders
		int32_t LMFrequency = LMEncoder.GetFrequency();
		int32_t RMFrequency = RMEncoder.GetFrequency();
		uint32_t target = targetSpeed;
		// Let's use the PID controller now (to track to the target rotation)
		//LMPID.SetOutputLimits(MinSpeedControlSpeed, MaxSpeedControlSpeed);
		LMPID.SetTunings(KpLMSpeed, KiLMSpeed, KdLMSpeed);
		//RMPID.SetOutputLimits(MinSpeedControlSpeed, MaxSpeedControlSpeed);
		RMPID.SetTunings(KpRMSpeed, KiRMSpeed, KdRMSpeed);

		// Turn on the PID controller now
		LMPID.SetMode(AUTOMATIC);
		RMPID.SetMode(AUTOMATIC);
		LMPIDSetpoint = (double)target;
		RMPIDSetpoint = (double)target;
		// The direction we are trying to go is always considered positive
		LMPIDInput = (double)LMFrequency;
		RMPIDInput = (double)RMFrequency;

		// Computer the Output using the PIDs
		LMPID.Compute();
		RMPID.Compute();

		// The direction we are trying to go is always considered positive
		uint16_t lmOutput = (uint16_t)(LMPIDOutput);
		uint16_t rmOutput = (uint16_t)(RMPIDOutput);

		// Set the left motor based on the PID output
		leftMotor.setSpeed(abs(lmOutput));
		if (lmOutput == 0)
		{
			leftMotor.run(RELEASE);
			leftMotor.setSpeed(0);
		}
		else if (targetDirection == Forward)
		{
			leftMotor.run(FORWARD);
		}
		else if (targetDirection == Backward)
		{
			leftMotor.run(BACKWARD);
		}

		// Set the right motor based on the PID output
		rightMotor.setSpeed(abs(rmOutput));
		if (rmOutput == 0)
		{
			rightMotor.run(RELEASE);
			rightMotor.setSpeed(0);
		}
		else if (targetDirection == Forward)
		{
			rightMotor.run(FORWARD);
		}
		else if (targetDirection == Backward)
		{
			rightMotor.run(BACKWARD);
		}
	}
		break;
	case NoControl:
	default:
		rightMotor.setSpeed(128);
		leftMotor.setSpeed(128);
		rightMotor.run(RELEASE);
		leftMotor.run(RELEASE);
		LMPID.SetMode(MANUAL);
		RMPID.SetMode(MANUAL);
		break;
	}
}

void SpeedController::DriveDistance(int32_t distance, MotorRunSetting_t direction)
{
	controllerType = DistanceControl;
	if ((direction == Forward) && (distance < 0))
	{
		targetDirection = Backward;
	}
	else if ((direction == Backward) && (distance < 0))
	{
		targetDirection = Forward;
	}
	else
	{
		targetDirection = direction;
	}
	if (distance == 0)
	{
		targetDirection = NoMotion;
		controllerType = NoControl;
	}
	targetDistance = abs(distance);
	targetRotation = 0;
	targetSpeed = 0;
	updateController = true;
}

void SpeedController::RotateDistance(int32_t rotation, MotorRunSetting_t direction)
{
	controllerType = RotationControl;
	if ((direction == Clockwise) && (rotation < 0))
	{
		targetDirection = Counterclockwise;
	}
	else if ((direction == Counterclockwise) && (rotation < 0))
	{
		targetDirection = Clockwise;
	}
	else
	{
		targetDirection = direction;
	}
	if (rotation == 0)
	{
		targetDirection = NoMotion;
		controllerType = NoControl;
	}

	targetRotation = abs(rotation);
	targetSpeed = 0;
	targetDistance = 0;
	updateController = true;
}

void SpeedController::DriveSpeed(int32_t speed, MotorRunSetting_t direction)
{
	controllerType = SpeedControl;
	targetDirection = direction;
	if ((direction == Forward) && (speed < 0))
	{
		targetDirection = Backward;
	}
	else if ((direction == Backward) && (speed < 0))
	{
		targetDirection = Forward;
	}
	else
	{
		targetDirection = direction;
	}
	if (speed == 0)
	{
		targetDirection = NoMotion;
		controllerType = NoControl;
	}

	targetSpeed = speed;
	targetDistance = 0;
	targetRotation = 0;
	updateController = true;
}

void SpeedController::Stop()
{
	targetDirection = NoMotion;
	targetSpeed = 0;
	targetDistance = 0;
	targetRotation = 0;
	controllerType = NoControl;
	rightMotor.setSpeed(0);
	leftMotor.setSpeed(0);
	rightMotor.run(RELEASE);
	leftMotor.run(RELEASE);
}
