#include "GPIO.h"

void GPIO::Init()
{
	// Set Digital Pin modes
	Digital::SetDigitalMode(Digital::GreenLED, Digital::Output);
	Digital::SetDigitalMode(Digital::OrangeLED, Digital::Output);
	Digital::SetDigitalMode(Digital::HeartbeatLED, Digital::Output);
	Digital::SetDigitalMode(Digital::SonarPing, Digital::Output);
}
