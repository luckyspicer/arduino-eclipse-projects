#ifndef _STANDARD_CONSTANTS_H
#define _STANDARD_CONSTANTS_H
#include "Arduino.h"

// Time Constants
#define	US_PER_MS	1000
#define MS_PER_SEC	1000
#define US_PER_SEC  1000000
#define SEC_PER_MIN 60
#define MIN_PER_HOUR 60
#define SEC_PER_HOUR 3600
#define HOUR_PER_DAY 24
#define SEC_PER_DAY 86400

inline uint32_t sec(void) { return (millis()/MS_PER_SEC); }
inline uint32_t ElapsedS(uint32_t start, uint32_t end) {
	return (((end) >= (start)) ? ((end) - (start)) : (0));
}

inline uint32_t ElapsedMS(uint32_t start, uint32_t end) {
	return (((end) >= (start)) ? ((end) - (start)) : (0));
}

inline uint32_t ElapsedUS(uint32_t start, uint32_t end) {
	return (((end) >= (start)) ? ((end) - (start)) : (0));
}

#endif
