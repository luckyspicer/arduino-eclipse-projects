#ifndef _PING_H_
#define _PING_H_

// Luke Spicer
// Spicer Robotics
// 2013-05-06

#include "Arduino.h"
#include "standardConstants.h"
#include "InterruptConstants.h"
#include "GPIO.h"

class Ping {
public:
	typedef enum {
		StartPing,
		ContinuePing,
	} PingCmd_t;
	static const uint32_t PulseMax_us = 18500;
	static const uint32_t PulseMin_us = 115;
	static const uint32_t TransitionTimeout_ms = 20;
	typedef void (*isrPtr)(void);
	Ping(InterruptConstants::InterruptNumber_t inter_num);
	uint32_t SendPing(PingCmd_t cmd);
	uint32_t GetPulseWidth(void);
	uint32_t GetPingState(void);

private:
	typedef enum {
		WaitForTrigger = 0,
		WaitForRising = 1,
		WaitForFalling = 2,
	} PingState_t;

	uint32_t interruptNum;
	GPIO::Digital::digitalPin_t triggerPin;
	volatile uint32_t pulseTimer;
	volatile uint32_t transitionTimer;
	volatile uint32_t pulseWidth;
	volatile PingState_t pingState;
};

#endif
