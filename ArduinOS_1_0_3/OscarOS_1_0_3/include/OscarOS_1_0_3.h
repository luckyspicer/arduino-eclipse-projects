// Only modify this file to include
// - function definitions (prototypes)
// - include files
// - extern variable definitions
// In the appropriate section

#ifndef OscarOS_1_0_3_H_
#define OscarOS_1_0_3_H_
#include "Arduino.h"
#include "StandardConstants.h"

//add your includes for the project OscarOS here
#include <DuinOS/DuinOS.h>
#include "GPIO.h"
#include "InterruptConstants.h"
#include "UIBoard.h"
#include "SpeedController.h"
#include "Ping.h"
#include "L3GD20.h"
#include "Wire.h"
#define Emic2 Serial2
#define XBee Serial3

//end of add your includes here
#ifdef __cplusplus
extern "C" {
#endif
void loop();
void setup();
#ifdef __cplusplus
} // extern "C"
#endif

//add your function definitions for the project OscarOS_1_0_3 here




//Do not add code below this line
#endif /* OscarOS_1_0_3_H_ */
