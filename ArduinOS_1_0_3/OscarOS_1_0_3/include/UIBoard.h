#ifndef _UIBOARD_H_
#define _UIBOARD_H_

// Includes
#include <UIButtons.h>
#include <InterruptConstants.h>
#include <glcd.h>
#include <fonts/Arial14.h>         // proportional font
#include <fonts/SystemFont5x7.h>   // system font
#include <bitmaps/ArduinoIcon.h>   // bitmap

class UIBoard {
public:
	UIBoard();
	void Begin(void);
	void Update(void);

private:
	// UI Buttons Object
	UIButtons displayButtons;
	uint32_t buttonPressTimes[UIButtons::NumberButtons];

	// GLCD display members
	uint32_t timer;
	glcd display;
	gText displayTextArea[UIButtons::NumberButtons];
	gText displayTimeArea;
	gText displayCliffArea;

	// UI Button Constants
	static const uint32_t updateTimer_ms = 1000;

};

#endif
