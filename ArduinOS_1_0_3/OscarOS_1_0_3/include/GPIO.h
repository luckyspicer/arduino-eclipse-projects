#ifndef _GPIO_H_
#define _GPIO_H_

#include "Arduino.h"
#include "standardConstants.h"

namespace GPIO
{

namespace Digital
{

typedef enum {
	pwm0 =			0,
	pwm1 =			1,
	pwm2 =			2,
	pwm3 =			3,
	pwm4 =			4,
	pwm5 =			5,
	pwm6 =			6,
	pwm7 =			7,
	pwm8 =			8,
	pwm9 =			9,
	pwm10 =			10,
	pwm11 =			11,
	pwm12 =			12,
	pwm13 =			13,
} pwmPin_t;
inline void SetPWM(pwmPin_t pin, uint16_t val) { analogWrite(pin, val); }

typedef enum {
	High = 			HIGH,
	Low = 			LOW,
} digitalValue_t;

typedef enum {
	Input =			INPUT,
	Output =		OUTPUT,
	Pullup =		INPUT_PULLUP,
} digitalPinMode_t;

typedef enum {
	digital0 =  	0,
	digital1 =  	1,
	digital2 =  	2,
	M2SpeedCntrl =  3,
	MotorBridge0 =  4,
	digital5 =  	5,
	digital6 =  	6,
	MotorBridge1 =  7,
	MotorBridge2 =  8,
	digital9 =  	9,
	digital10 = 	10,
	M1SpeedCntrl = 	11,
	MotorBridge3 = 	12,
	HeartbeatLED = 	13,
	digital14 = 	14,
	digital15 = 	15,
	digital16 = 	16,
	digital17 = 	17,
	SonarPing = 	18,
	UIButtonInt =	19,
	digital20 = 	20,
	digital21 = 	21,
	GLCDDB0 =		22,
	GLCDDB1 =		23,
	GLCDDB2 =		24,
	GLCDDB3 =		25,
	GLCDDB4 =		26,
	GLCDDB5 =		27,
	GLCDDB6 =		28,
	GLCDDB7 =		29,
	digital30 = 	30,
	digital31 = 	31,
	digital32 = 	32,
	GLCDCS1 =		33,
	GLCDCS2 =		34,
	GLCDRW =		35,
	GLCDRS =		36,
	GLCDEnable = 	37,
	LeftButton = 	38,
	UpButton = 		39,
	DownButton = 	40,
	RightButton = 	41,
	digital42 = 	42,
	SelectButton = 	43,
	OrangeLED = 	44,
	GreenLED = 		45,
	RearCliff = 	46,
	FrontCliff = 	47,
	digital48 = 	48,
	digital49 = 	49,
	REncoderDir = 	50,
	LEncoderDir = 	51,
	REncoderClk = 	52,
	LEncoderClk = 	53,
	digitalA0 = 	A0,
	digitalA1 = 	A1,
	digitalA2 = 	A2,
	digitalA3 = 	A3,
	digitalA4 = 	A4,
	digitalA5 = 	A5,
	digitalA6 = 	A6,
	digitalA7 = 	A7,
	digitalA8 = 	A8,
	digitalA9 = 	A9,
	digitalA10 = 	A10,
	digitalA11 = 	A11,
	digitalA12 = 	A12,
	digitalA13 = 	A13,
	digitalA14 = 	A14,
	digitalA15 = 	A15,
} digitalPin_t;
inline void SetDigitalMode(digitalPin_t pin, digitalPinMode_t mode) { pinMode(pin, mode); }
inline uint16_t ReadDigital(digitalPin_t pin) { return (digitalRead(pin)); }
inline void SetDigital(digitalPin_t pin, digitalValue_t val) { digitalWrite(pin, val); }
}; // namespace Digital

namespace Analog
{

typedef enum {
	analog0 =		0,
	analog1 =		1,
	analog2 =		2,
	analog3 =		3,
	analog4 =		4,
	analog5 =		5,
	analog6 =		6,
	analog7 =		7,
	analog8 =		8,
	analog9 =		9,
	analog10 = 		10,
	analog11 =		11,
	analog12 = 		12,
	analog13 =		13,
	analog14 =		14,
	analog15 =		15,
} analogPin_t;
inline uint16_t ReadADC(analogPin_t pin) { return (analogRead(pin)); }

typedef enum {
	Default = DEFAULT,
	// Internal = INTERNAL, not available on the ArduinoMega2560
	Internal1V1 = INTERNAL1V1,
	Internal2V56 = INTERNAL2V56,
	External = EXTERNAL
} analogReference_t;
inline void SetADCReference(analogReference_t refType) { analogReference(refType); }

}; // namespace Analog

void Init(void);

}; //  namespace GPIO
#endif
