#ifndef _SPEED_CONTROLLER_H_
#define _SPEED_CONTROLLER_H_

// Includes
#include "Arduino.h"
#include "StandardConstants.h"
#include "GPIO.h"
#include "WheelEncoder.h"
#include "AFMotor.h"
#include "PID_v1.h"

#define RMEncoder WheelEncoder0
#define LMEncoder WheelEncoder1

class SpeedController {
public:
	// Enums and Constants
	typedef enum {
		Forward,
		Backward,
		Clockwise,
		Counterclockwise,
		NoMotion
	} MotorRunSetting_t;

	static const uint32_t wheelRadiusx10_mm = 346;		// Measured wheel radius 34.6mm
	static const uint32_t pulsesPer100Rotations = 12800; // 128 pulses per rotation
	static const uint32_t PI_NUMERATOR = 31416;			// pi x 10000
	static const uint32_t PI_DENOMENATOR = 10000;
	static const uint32_t mmPer100Rotations = ((2 * PI_NUMERATOR * wheelRadiusx10_mm * 100) / (10 * PI_DENOMENATOR));
	static const uint32_t pulsePerDegreeNumerator = 67432;
	static const uint32_t pulsePerDegreeDenomenator = 100000;

	SpeedController();
	void Begin(void);
	void Update(void);
	void DriveSpeed(int32_t speed, MotorRunSetting_t direction);
	void DriveDistance(int32_t distance, MotorRunSetting_t direction);
	void RotateDistance(int32_t rotation, MotorRunSetting_t direction);
	void Stop(void);
	static int32_t MMToPulses(int32_t distance_mm) { return ((distance_mm * pulsesPer100Rotations) / (mmPer100Rotations)); }
	static int32_t DegreeToPulses(int32_t rotation_degrees) { return ((rotation_degrees * pulsePerDegreeNumerator) / (pulsePerDegreeDenomenator)); }

private:
	// Constants
	static const uint8_t LMPort = 1;
	static const uint8_t LMFrequency = MOTOR12_64KHZ;
	static const uint8_t RMPort = 2;
	static const uint8_t RMFrequency = MOTOR12_64KHZ;
	static const uint8_t TargetDistanceSpeed = 128;
	static const int32_t distanceMargin = 8;
	static const double KpPos = 29.4;	// https://controls.engin.umich.edu/wiki/index.php/PIDTuningClassical
	static const double KiPos = 202.76; // Values Calculated using the Ziegler-Nichols closed-loop tuning method
	static const double KdPos = 1.065;	//
	static const double KpRot = 29.4;	// We can use the same values for the rotation controller
	static const double KiRot = 202.76;	//
	static const double KdRot = 1.065;	//
	static const double KpLMSpeed = 1.0;	// ??
	static const double KiLMSpeed = 0.0;	// ??
	static const double KdLMSpeed = 0.0;	// ??
	static const double KpRMSpeed = 1.2;	// ??
	static const double KiRMSpeed = 0.0;	// ??
	static const double KdRMSpeed = 0.0;	// ??
	static const int16_t PIDSamplePeriod_ms = 25;
	static const double MinSpeed = -255.0;
	static const double MaxSpeed = 255.0;
	static const double MinSpeedControlSpeed = 60.0;
	static const double MaxSpeedControlSpeed = 255.0;
	static const uint8_t MinMovingSpeed = 50;

	typedef enum {
		SpeedControl,
		DistanceControl,
		RotationControl,
		NoControl
	} SpeedControllerType_t;

	// Private Members
	AF_DCMotor leftMotor;
	AF_DCMotor rightMotor;
	MotorRunSetting_t targetDirection;
	uint32_t targetSpeed;
	uint32_t targetDistance;
	uint32_t targetRotation;
	bool updateController;
	SpeedControllerType_t controllerType;
	SpeedControllerType_t lastControllerType;
	double LMPIDInput;
	double RMPIDInput;
	double LMPIDOutput;
	double RMPIDOutput;
	double LMPIDSetpoint;
	double RMPIDSetpoint;
	double Kp;
	double Ki;
	double Kd;
	PID LMPID;
	PID RMPID;

	// Private Methods
};

#endif
